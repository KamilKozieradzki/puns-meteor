import { Meteor } from 'meteor/meteor';
import FacebookOAuthInit from '../imports/api/oauth-facebook';
import '../imports/api/notifications';
import Categories from '../imports/collections/categories';
import Games from '../imports/collections/games';
import Pictures from '../imports/collections/pictures';
import Answers from '../imports/collections/answers';

import '../imports/api/categories/methods'
import '../imports/api/games/methods'
import '../imports/api/guesses/methods'
import '../imports/api/images/methods'
import '../imports/api/users/methods'

Meteor.startup(() => {
  // code to run on server at startup
  FacebookOAuthInit();
});
