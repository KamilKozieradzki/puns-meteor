import React, { Component } from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import { Meteor } from 'meteor/meteor';

class Dashboard extends Component {

  constructor(props) {
    super(props);
    this.state = {
      user: '',
      categoryAdd: '',
      categoryDelete: '',
      puzzleDelete: '',
      puzzleAdd: ''
    }
  }

  _renderUserRows() {
    return this.props.users.map( user => {
      return (
        <tr key={user._id}>
          <td>
            <div>Name: {user.profile.name}</div>
            <div>Id: {user._id}</div>
          </td>
          <td>
            <button onClick={()=>{
              const settings = {
                mode: 3,
                users: Meteor.users.find({_id: {$not: user._id}}, {limit: 2}).fetch().map(user => user.services.facebook.id),
              }
              Meteor.call('newPendingGame', settings, user._id, (err, res) => {
                console.log(err, res);
              })}}>
              New game
            </button>
          </td>
        </tr>
      );
    })
  }

  handleUserChange(event) {
    this.setState({user: event.target.value});
  }

  handleAddPuzzleChenge(event) {
    this.setState({puzzleAdd: [event.target.placeholder, event.target.value]});
  }

  handleAddPuzzle(event) {
    event.preventDefault();
    Meteor.call('addPuzzleToCategory', this.state.puzzleAdd[0], this.state.puzzleAdd[1], (err, res) => {
      console.log(err, res);
    });
  }

  handleDeletePuzzleChenge(event) {
    this.setState({puzzleDelete: [event.target.placeholder, event.target.value]});
  }

  handleDeletePuzzle(event) {
    event.preventDefault();
    Meteor.call('deletePuzzleFromCategory', this.state.puzzleDelete[0], this.state.puzzleDelete[1], (err, res) => {
      console.log(err, res);
    });
  }

  handleAddCategoryChenge(event) {
    this.setState({categoryAdd: event.target.value});
  }

  handleAddCategory(event) {
    event.preventDefault();
    Meteor.call('addNew', this.state.categoryAdd, (err, res) => {
      console.log(err, res);
    });
  }

  handleDeleteCategoryChenge(event) {
    this.setState({categoryDelete: event.target.value});
  }

  handleDeleteCategory(event) {
    event.preventDefault();
    Meteor.call('deleteCategory', this.state.categoryDelete, (err, res) => {
      console.log(err, res);
    });
  }

  _renderGameRows() {
    return this.props.games.map( game => {
      let rowClass;
      if(game.cancelled) {
        rowClass = 'table-danger';
      } else if(game.pending) {
        rowClass = 'table-success';
      } else {
        rowClass = 'table-info';
      }
      return (
        <tr key={game._id} className={rowClass}>
          <td>
            <div>{game._id}</div>
          </td>
          <td>Pending: {game.pending? 'true' : 'false'}, Cancelled: {game.cancelled? 'true' : 'false'}</td>
          <td>
            <select onChange={(event)=>{this.handleUserChange(event)}}>
              <option value=""></option>
              {
                game.users.map( user => {
                  return (
                    <option key={user.id} value={user.id}>
                      {user.id}
                    </option>
                  );
                })
              }
            </select>
          </td>
          <td>
            <button onClick={()=>{Meteor.call('acceptInvitation', game._id, this.state.user, (err, res) => {
              console.log(err, res);
              })}}>
              Accept inv
            </button>
            <button onClick={()=>{Meteor.call('declineInvitation', game._id, (err, res) => {
              console.log(err, res);
              })}}>
              Decline inv
            </button>
          </td>
        </tr>
      );
    })
  }

  _renderCategories() {
    return this.props.categories.map( category => {
      return (
        <tr key={category._id}>
          <td>
            Name: {category.name} 
            Id: {category._id}
          </td>
          <td>
            {category.puzzles.join(', ')}
          </td>
          <td>
            <form onSubmit={(event) => {this.handleAddPuzzle(event)}}>
              <input type="text" placeholder={category.name} onChange={(event) => this.handleAddPuzzleChenge(event)}/>
              <input type="submit" value="Add new" />
            </form>
          </td>
          <td>
            <form onSubmit={(event) => {this.handleDeletePuzzle(event)}}>
              <input type="text" placeholder={category.name} onChange={(event) => this.handleDeletePuzzleChenge(event)}/>
              <input type="submit" value="Delete" />
            </form>
          </td>
        </tr>
      );
    })
  }

  render() {
    return (
      <div className="animated fadeIn">

        <div className="row">
          <div className="col-md-12">
            <div className="card">
              <div className="card-header">
                Users
              </div>
              <div className="card-block">
                <table className="table table-hover table-outline mb-0 hidden-sm-down">
                  <thead className="thead-default">
                    <tr>
                      <th>User</th>
                      <th>Activity</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this._renderUserRows()}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

        <div className="row">
          <div className="col-md-12">
            <div className="card">
              <div className="card-header">
                Games
              </div>
              <div className="card-block">
                <table className="table table-hover table-outline mb-0 hidden-sm-down">
                  <thead className="thead-default">
                    <tr>
                      <th>Game</th>
                      <th>Game status</th>
                      <th>User</th>
                      <th>Activity</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this._renderGameRows()}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

      <div className="row">
          <div className="col-md-12">
            <div className="card">
              <div className="card-header">
                <div className="row">
                  <div className="col-md-4">
                    <p>Categories</p>
                  </div>
                  <div className="col-md-4">
                    <form onSubmit={(event) => {this.handleAddCategory(event)}}>
                      <input type="text" value={this.state.categoryAdd} onChange={(event) => {this.handleAddCategoryChenge(event)}} />
                      <input type="submit" value="Add new" />
                    </form>
                  </div>
                  <div className="col-md-4">
                    <form onSubmit={(event) => {this.handleDeleteCategory(event)}}>
                      <input type="text" value={this.state.categoryDelete} onChange={(event) => {this.handleDeleteCategoryChenge(event)}} />
                      <input type="submit" value="Delete category" />
                    </form>
                  </div>
                </div>
              </div>
              <div className="card-block">
                <table className="table table-hover table-outline mb-0 hidden-sm-down">
                  <thead className="thead-default">
                    <tr>
                      <th>Category</th>
                      <th>Puzzles</th>
                      <th>Add new</th>
                      <th>Delete</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this._renderCategories()}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default createContainer(() => {
  return {
    users: Meteor.users.find({}).fetch(),
    games: Games.find({}).fetch(),
    categories: Categories.find({}).fetch()
  }
}, Dashboard);