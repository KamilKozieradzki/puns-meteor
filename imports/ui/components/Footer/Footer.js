import React, { Component } from 'react';

class Footer extends Component {
  render() {
    return (
      <footer className="app-footer">
        <a href="http://kozieradzki.pl">GyessMaster!</a> &copy; 2017 Kamil Kozieradzki.
      </footer>
    )
  }
}

export default Footer;
