Meteor.methods({
    registerDevice(deviceToken) {
        return Meteor.users.update({_id: Meteor.userId()}, {
            $set: {
                deviceToken
            }
        })
    },
    changeLanguage(language) {
        return Meteor.users.update({_id: Meteor.userId()}, {
            $set: {
                "profile.language": language
            }
        })
    }
})