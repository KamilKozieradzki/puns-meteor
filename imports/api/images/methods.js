import ImagesModel from './model.js'
Meteor.methods({
    updateImage(image, imageId, gameId) {
        return ImagesModel.updateImage(image, imageId, gameId);
    },

    saveEmptyImage(gameId, puzzle) {
        if(!gameId || !puzzle) {
            throw new Meteor.Error('image or game undefined', gameId, puzzle)
        }
        return ImagesModel.saveImage(undefined, gameId, puzzle);
    }
})