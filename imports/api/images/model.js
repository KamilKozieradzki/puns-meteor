import GamesModel from '../games/model.js'

export default class ImagesModel {
    constructor() {

    }
    static saveImage(image, gameId, puzzle) {
        const game = Games.findOne({
            _id: gameId
        });
        const author = game.queue[0]
        const round = game.round
        const insert = Pictures.insert({
            image,
            gameId,
            author,
            round,
            puzzle,
            creacted: Date.now()
        })
        return insert;
    }

    static updateImage(image, imageId, gameId) {
        const game = Games.findOne({
            _id: gameId
        });
        const insert = Pictures.update({_id: imageId}, {
            $set: {image}
        })
        const round = game.round
        const count = Pictures.find({gameId, round}).count();
        if(count === game.settings.pictures) {
            let usersQueue = Games.findOne({_id: gameId}).queue
            usersQueue[0] = {
                userId: usersQueue[0].userId,
                status: 'pending'
            }
            Games.update(
                {
                    _id: gameId
                }, 
                {
                    $set: {
                        "queue": usersQueue,                    }
                });
            GamesModel.notifyYouCanGuess(Pictures.findOne({_id: imageId}).gameId);
        }
        return insert;
    }
}