import { ServiceConfiguration } from 'meteor/service-configuration';
import { Meteor } from 'meteor/meteor';

const facebook = JSON.parse(Assets.getText('settings.json')).oauth.facebook;

const init = () => {
  if (!facebook) return;
  ServiceConfiguration.configurations.upsert(
    { service: "facebook" },
    {
      $set: {
        appId: facebook.appId,
        secret: facebook.secret
      }
    }
  );
  registerHandler();
}

const registerHandler = () => {
  Accounts.registerLoginHandler('facebook', function(params) {
    console.log(params.facebook);
    const data = params.facebook;

    // If this isn't facebook login then we don't care about it. No need to proceed.
    if (!data) {
      return undefined;
    }

    // The fields we care about (same as Meteor's)
    const whitelisted = ['friends', 'id', 'email', 'name', 'first_name',
     'last_name', 'link', 'gender', 'locale', 'age_range'];

    // Get our user's identifying information. This also checks if the accessToken
    // is valid. If not it will error out.
    const identity = getIdentity(data.accessToken, whitelisted);
    // Get our user's friends information.
    const friends = getFriends(data.accessToken, identity);

    // Build our actual data object.
    const serviceData = {
      accessToken: data.accessToken,
      expiresAt: (+new Date) + (1000 * data.expirationTime)
    };
    const fields = Object.assign({}, serviceData, identity);

    // Search for an existing user with that facebook id
    const existingUser = Meteor.users.findOne({ 'services.facebook.id': identity.id });

    let userId;
    if (existingUser) {
      userId = existingUser._id;

      // Update our data to be in line with the latest from Facebook
      const prefixedData = {};
      _.each(fields, (val, key) => {
        prefixedData[`services.facebook.${key}`] = val;
      });

      Meteor.users.update({ _id: userId }, {
        $set: prefixedData,
      });

      if(!existingUser.profile.language) {
        Meteor.users.update({ _id: userId }, {
          $set: {"profile.language": identity.locale.substr(0, 2)},
        });
      }

      if(identity.email) {
        Meteor.users.update({ _id: userId }, {
          $addToSet: { emails: { address: identity.email, verified: true } }
        });
      }

    } else {
      // Create our user
      userId = Meteor.users.insert({
        services: {
          facebook: fields
        },
        profile: { 
          name: identity.name,
          language: identity.locale.substr(0, 2) || 'english'
        }
      });

      if(identity.email) {
        Meteor.users.update({ _id: userId }, {
          $addToSet: { emails: { address: identity.email, verified: true } }
        });
      }
      
    }

    return { userId: userId };
  });
};

// Gets the identity of our user and by extension checks if
// our access token is valid.
const getIdentity = (accessToken, fields) => {
  try {
    return HTTP.get("https://graph.facebook.com/v2.9/me", {
      params: {
        access_token: accessToken,
        fields: fields.join(',')
      }
    }).data;
  } catch (err) {
    throw _.extend(new Error("Failed to fetch identity from Facebook. " + err.message),
                   {response: err.response});
  }
};

const getFriends = (accessToken, identity) => {
  try {
    // console.log(`https://graph.facebook.com/v2.9/${identity.id}/friends`);
    return HTTP.get(`https://graph.facebook.com/v2.9/${identity.id}/friends`, {
      params: {
        access_token: accessToken
      }
    }).data;
  } catch (err) {
    throw _.extend(new Error("Failed to fetch identity from Facebook. " + err.message),
                   {response: err.response});
  }
};

export default init;