import CategoryModel from './model.js'
Meteor.methods({
    addNew(category) {
        if(!category) {
            throw new Meteor.Error('category undefined', category);
        }
        return CategoryModel.addNewCategory(category);
    },
    deleteCategory(category) {
        if(!category) {
            throw new Meteor.Error('category undefined', category);
        }
        return CategoryModel.deleteCategory(category);
    },
    addPuzzleToCategory(category, puzzle) {
        if(!category || !puzzle) {
            throw new Meteor.Error('category or puzzle undefined', category, puzzle)
        }
        return CategoryModel.addPuzzleToCategory(category, puzzle);
    },

    deletePuzzleFromCategory(category, puzzle) {
        if(!category || !puzzle) {
            throw new Meteor.Error('category or puzzle undefined', category, puzzle)
        }
        return CategoryModel.deletePuzzleFromCategory(category, puzzle);
    }
    
})