export default class CategoryModel {
    constructor() {

    }
    static addNewCategory(category) {
        return Categories.insert({
            name: category,
            puzzles: [],
            language: 'pl'
        });
    }
    static deleteCategory(category) {
        return Categories.remove({
            name: category
        });
    }
    static addPuzzleToCategory(category, puzzle) {
        return Categories.update({ name: category },{ $push: { puzzles: puzzle }})
    }

    static deletePuzzleFromCategory(category, puzzle) {
        let puzzles = Categories.findOne({name: category}).puzzles;
        let index = puzzles.indexOf(puzzle);
        if(index !== -1) {
            puzzles.splice(index, 1);
            let update = Categories.update({ name: category },{ $set: { puzzles }});
            return update;
        }
        return index;
    }
}