import GuessModel from './model.js'
Meteor.methods({
    handleGuess(answerId, answer, pictureId) {
        if(!answerId || !answer) {
            throw new Meteor.Error('userId || pictureId undefined', answerId, answer)
        }
        return GuessModel.handleGuess(answerId, answer, pictureId);
    },

    initGuess(userId, pictureId) {
        if(!userId || !pictureId) {
            throw new Meteor.Error('userId || pictureId undefined', userId, pictureId)
        }
        return GuessModel.initGuess(userId, pictureId, 'sdfsdfsdfsdfsdf');
    }
})