import GamesModel from '../games/model.js'
export default class GuessModel {
    constructor() {

    }
    static initGuess(userId, pictureId, answer) {
        const picture  = Pictures.findOne({_id: pictureId});
        let isCorrect = undefined;
        if(answer) {
             isCorrect = picture.puzzle === answer;
        }
        let isThereAnswer = Answers.findOne({userId, pictureId, round: picture.round, gameId: picture.gameId});
        if(isThereAnswer) {
            return;
        }
        const insert = Answers.insert({
            userId,
            pictureId,
            isCorrect,
            round: picture.round,
            puzzle: picture.puzzle,
            gameId: picture.gameId,
        })
        return insert;
    }

    static handleGuess(answerId, answer, pictureId) {
        const picture  = Pictures.findOne({_id: pictureId});
        let isCorrect = undefined;
        if(answer) {
             isCorrect = picture.puzzle === answer;
        }
        Answers.update({
            _id: answerId
        }, {
            $set: {
                isCorrect
            }
        })
        if(isCorrect) {
            let places = Games.findOne({_id: picture.gameId}).places;
            if(!places) {
                places = {};
            }
            let guesser = Answers.findOne({_id: answerId}).userId;
            if(!places[guesser]) {
                places[guesser] = {
                    points: 0
                }
            }
            if(!places[picture.author.userId]) {
                places[picture.author.userId] = {
                    points: 0
                }
            }
            places[guesser].points = places[guesser].points + 1 || 1
            places[picture.author.userId].points = places[picture.author.userId].points + 1 || 1
            Games.update({
                _id: picture.gameId
            }, {
                $set: {places}
            })
        }
        GamesModel.handleFinishGuessing(picture);
        return isCorrect;
    }
}