export default class GamesModel {
    constructor() {

    }
    static createPending(settings, userId) {
        if(!this.settingsValidation(settings)){
            return false;
        }
        // Rounds from 0!
        settings.rounds = settings.rounds || settings.users.length;
        settings.pictures = settings.pictures || 3;
        settings.categories = settings.categories || 3;
        switch(settings.mode) {
            case 2:
                settings.mode = {
                    id: 2,
                    modeName: 'Duel'
                }
                return this.createDuelGame(settings, userId);
                break;
            case 3:
                settings.mode = {
                    id: 3,
                    modeName: 'Group'
                }
                return this.createGroupGame(settings, userId);
                break;
            case 4:
                settings.mode = {
                    id: 4,
                    modeName: 'Team'
                }
                return this.createTeamGame(settings, userId);
                break;
            default:
                throw new Meteor.Error('Validation failed! This should not be happened!')
        }
        return true;
    }
    static settingsValidation(settings) {
        const users = settings.users;
        const usersCount = users.length;
        const mode = settings.mode;

        // if solo there is 0 players || solo mode = 1
        const duelModeUsers = 1; // + 1 everywhere ||  duel mode = 2
        const groupModeUsers = 2 // same || group mode = 3
        const teamMoreUsers = 3; // same || team mode = 4

        if(usersCount < duelModeUsers) {
            throw new Meteor.Error('Not enough players!')
        } else if(usersCount >= duelModeUsers && mode > 1) {
            if(usersCount === duelModeUsers && mode === 2) {
                return true;
            } else if(usersCount >= groupModeUsers && mode === 3) {
                return true;
            } else if(usersCount === teamMoreUsers && mode === 4) {
                return true;
            } else {
                throw new Meteor.Error(`Tou choose ${usersCount} player(s) on mode ${mode}. This should be valideted localy!`);
            }
        } else {
            throw new Meteor.Error('Not enough players or some way you try to play solo!')
        }
    }

    static findGameUsersFromFacebook(users) {
        return users.map( userId => {
            return {
                id: Meteor.users.findOne({"services.facebook.id": userId})._id,
                joined: false
            }
        });
    }

    static createDuelGame(settings) {
        return true;
    }

    static createGroupGame(settings, userId) {
        let users = this.findGameUsersFromFacebook(settings.users);
        const hostName = Meteor.users.findOne({_id: userId}).profile.name;
        this.inviteToGameNotification(hostName, users.map(user => user.id));
        users.push({
            id: userId || Meteor.userId(),
            joined: true
        })
        const gameId = Games.insert({
            settings,
            users,
            mode: settings.mode,
            host: userId || Meteor.userId(),
            createdAt: new Date(),
            pending: true,
            finished: false,
            cancelled: false,
            places: {}
        });
        return true;
    }

    static createTeamGame(settings) {
        return true;
    }

    static acceptIntitation(gameId, userId) {
        Games.update({"users.id": userId, _id: gameId}, {$set: {"users.$.joined": true}})
        const isReady = !!!Games.findOne({_id: gameId, "users.joined": false});
        if(isReady) {
            const game = Games.findOne({_id: gameId});
            let usersQueue = game.users.map( user => user.id )
            const userIndex = usersQueue.indexOf(userId);
            usersQueue.splice(userIndex, 1);
            usersQueue.unshift(userId);
            const queue = usersQueue.map( userId => {
                return {
                    userId,
                    status: ''
                }
            })
            // Get number of categories (it is in settings)
            const categories = this.getRandom(Categories.find({language: Meteor.user().profile.language}).fetch(), game.settings.categories);
            return Games.update(
                {
                    _id: gameId
                }, 
                {
                    $set: {
                        "pending": false,
                        "queue": queue,
                        "round": 0,
                        "categoryProps": categories,
                        "roundData": [
                            {
                                _id: 0,
                                author: queue[0].userId
                            }
                        ]
                    }
                });
        } else {
            return false;
        }
    }

    static declineIntitation(gameId) {
        return Games.update({_id: gameId}, {$set: {cancelled: true}})
    }

    static deleteCategoryProps(gameId) {
        return Games.update(
                {
                    _id: gameId
                }, 
                {
                    $set: {
                        "categoryProps": undefined
                    }
                })
    }

    static setCategory(category, gameId) {
        if(!this.deleteCategoryProps(gameId)) {
            throw new Meteor.Error('Could not delete category props');
        }
        const game = Games.findOne({_id: gameId});
        const round = game.round;
        const wrongAnswers = 3;
        let puzzles = this.getRandom(category.puzzles, game.settings.pictures).map(puzzle => {
            return {
                puzzle,
                answers: this.getRandom(category.puzzles, wrongAnswers, puzzle)
            }
        });
        return Games.update(
                {
                    _id: gameId,
                    "roundData._id": round
                }, 
                {
                    $set: {
                        "roundData.$.category": category,
                        "roundData.$.puzzles": puzzles
                    }
                });
    }

    static getRandom(arr, n, not) {
        const result = new Array(n);
        if(not) {
            let index = arr.indexOf(not);
            arr.splice(index, 1)
        }
        let len = arr.length;
        let taken = new Array(len);
        if (n > len)
            throw new RangeError("getRandom: more elements taken than available");
        while (n--) {
            var x = Math.floor(Math.random() * len);
            result[n] = arr[x in taken ? taken[x] : x];
            taken[x] = --len;
        }
        return result
    }

    static shuffle(array) {
        var currentIndex = array.length, temporaryValue, randomIndex;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }

        return array;
    }

    static handleFinishGuessing(picture) {
        // check if player answered all puns
        const game = Games.findOne({_id: picture.gameId});
        const count = Answers.find({gameId: picture.gameId, round: picture.round, userId: Meteor.userId()}).count();
        if(count === game.settings.pictures) {
            // if so, he finished his turn. Check his nuber and add him pending status
            let usersQueue = Games.findOne({_id: picture.gameId}).queue
            let userNum;
            usersQueue.map( (user, index) => {
                if(user.userId === Meteor.userId()) {
                    userNum = index;
                }
            })
            usersQueue[userNum] = {
                userId: usersQueue[userNum].userId,
                status: 'pending'
            }
            Games.update(
                {
                    _id: picture.gameId
                }, 
                {
                    $set: {
                        "queue": usersQueue
                    }
                });
            
            
            // Finish round or game!
            if(picture.round === game.settings.rounds) {
                this.handleFinishGame(picture.gameId);
            } else {
                this.handleRoundFinish(picture.gameId);
            }
        }
    }

    static handleRoundFinish(gameId) {
        // Check if all players finished their job, if so start new round and reset their status
        const game = Games.findOne({
            _id: gameId
        });

        const pendingUsers = game.queue.filter( user => {
            if(user.status === 'pending') {
                return user;
            }
        })
        
        if(pendingUsers.length === game.queue.length) {
            console.log('round change is happening');
            const categories = this.getRandom(Categories.find({language: Meteor.user().profile.language}).fetch(), game.settings.categories);
            const queue = game.queue.map( user => {
                return {
                    userId: user.userId,
                    status: ''
                }
            })
            // Get first user and add to be lat
            const shiftedUser = queue.shift();
            queue.push(shiftedUser);

            Games.update({
                _id: gameId
            },
            {
                $inc: {
                    round: 1
                },
                $set: {
                    queue: queue,
                    "categoryProps": categories,
                },
                $addToSet: {
                    "roundData": {
                                _id: game.round + 1,
                                author: queue[0].userId
                    }
                }
            })
            this.notifyYouCanDraw([queue[0].userId]);
        }
    }

    static handleFinishGame(gameId) {
        const game = Games.findOne({
            _id: gameId
        });

        const pendingUsers = game.queue.filter( user => {
            if(user.status === 'pending') {
                return user;
            }
        })

        if(pendingUsers.length === game.queue.length) {
            let places = game.places;
            places = Object.keys(places).map(function(e) {
                return [e, places[e].points];
            });
            places.sort(function compare(a,b) {
                if (a.points < b.points)
                    return -1;
                if (a.points > b.points)
                    return 1;
                return 0;
            })

            Games.update({
                _id: gameId
            }, {
                $set: {
                    places,
                    finished: true
                }
            });
        }
    }

    static initCards(game) {
        let res = game.roundData.map((round, index) => {
            let answerData = game.users.map( user => {
                let points = Answers.find({gameId: game._id, userId: user.id, round: round._id});
                if(user.id === round.author) {
                    const authorPoints = Answers.find({gameId: game._id, round: round._id, isCorrect: true});
                    return {
                        userId: user.id,
                        points: [authorPoints.count()]
                    };
                } else if(!game.roundData[index + 1] && !game.finished) {
                    points = new Array(game.settings.pictures);
                    return {
                        userId: user.id,
                        points
                    }
                } else if (points.count() === game.settings.pictures){
                    return {
                        userId: user.id,
                        points: points.fetch().map(point => point.isCorrect)
                    }
                } else {
                    points = new Array(game.settings.pictures);
                    return {
                        userId: user.id,
                        points
                    }
                }
            });
            let authorName = Meteor.users.findOne({_id: round.author}).profile.name;
            const data = {
                author: round.author,
                authorName,
                round: round._id,
                category: (round.category ? round.category.name: null),
                answerData
            }
            return data;
        })
        if(!game.finished) {
            let temp;
            res.map((round, index)=> {
                if(round.round === game.round) {
                    temp = index;
                }
            })
            let first = res.splice(temp, 1);
            res.unshift(first[0]);
        } else {
            let finalCard = game.places.map( (place, index) => {
                if(place[0]) {
                    return {
                        place: index + 1,
                        points: parseInt(place[1], 10),
                        name: Meteor.users.findOne({_id: place[0]}).profile.name
                    }
                }
                
            })
            res.unshift(finalCard);
        }
        return res;
    }

    static inviteToGameNotification(hostName, players) {
        const data = {
            headings: {
                en: 'New Invitation!'
            },
            contents: {
                en: hostName + ' has invited you to new game!',  
            }
            // data: {gameId: 'igHesSmHiFxW7HQYG'},
            // buttons: [{"id": "id1", "text": "button1", "icon": "ic_menu_share"}, {"id": "id2", "text": "button2", "icon": "ic_menu_send"}]
        };
        Meteor.call('sendNotification', players, data);
    }

    static notifyYouCanGuess(gameId) {
        let players = Games.findOne({_id: gameId}).queue;
        players.shift()
        players.map(user => {
            return user.userId;
        })
        const data = {
            headings: {
                en: 'It is your turn!'
            },
            contents: {
                en: 'You can guess now!!',  
            }
        };
        Meteor.call('sendNotification', players, data);
    }

    static notifyYouCanDraw(players) {
        const data = {
            headings: {
                en: 'It is your turn!'
            },
            contents: {
                en: 'You should set category and draw!',  
            }
        };
        console.log(players);
        Meteor.call('sendNotification', players, data);
    }
}