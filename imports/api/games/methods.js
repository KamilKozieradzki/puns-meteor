import GamesModel from './model.js'
Meteor.methods({
    newPendingGame(settings, userId) {
        if(!settings) {
            throw new Meteor.Error('Something went wrong with your settings!');
        }
        userId = userId || Meteor.userId();
        return GamesModel.createPending(settings, userId);
    },

    acceptInvitation(gameId, userId) {
        if(!gameId) {
            throw new Meteor.Error('Something went wrong!');
        }
        userId = userId || Meteor.userId();
        return GamesModel.acceptIntitation(gameId, userId);
    },

    declineInvitation(gameId) {
        if(!gameId) {
            throw new Meteor.Error('Something went wrong!');
        }
        return GamesModel.declineIntitation(gameId);
    },

    setCategory(category, gameId) {
        if(!category || !gameId) {
            throw new Meteor.Error('category or game undefined', category, gameId)
        }
        return GamesModel.setCategory(category, gameId);
    },

    initCards(game) {
        return GamesModel.initCards(game);
    }
})