SentNotifications = new Mongo.Collection('oneSignalNotifications');

const settings = {
    "oneSignal": {
        "apiKey": "MzcxZjFjOGEtM2NhOS00ODQ1LWJiMWItNDZiYmQ4MjQ4MTJl",    
        "appId": "2ce30379-4d2a-4025-a30a-a02540210bec"
    }
}

OneSignal = {
  _base: 'https://onesignal.com/api/v1/',
  send(method, api, data){
    const url = `${this._base}/${api}`;
    const { apiKey } = settings.oneSignal;

    return HTTP.call(method, url, {
      data,
      headers: {
        Authorization: `Basic ${apiKey}`,
      },
    });
  },
};

OneSignal.Notifications = {
  _api: 'notifications',
  create(players, data){
    const url = `${this._api}`;
    const { appId } = settings.oneSignal;

    SentNotifications.insert({
      ...data,
      createdAt: new Date(),
    });

    return OneSignal.send('POST', url, {
      ...data,
      app_id: appId,
      include_player_ids: players,
    });
  },
};

Meteor.methods({
    sendNotification(usersIds, data) {
        let playersId = Meteor.users.find({"_id": { "$in": usersIds }}).fetch().map( user => {
            if(user.deviceToken) {
                return user.deviceToken.userId;
            }
        });
        playersId = playersId.filter(function( element ) {
            return element !== undefined;
        });
        console.log(playersId);
        if(playersId.length) {
            OneSignal.Notifications.create(playersId, data);
        }
    }

})

// const data = {
//     headings: {
//         en: 'Wazup!'
//     },
//     contents: {
//         en: 'Hey! Wazup? We miss you.',  
//     }
//     // data: {gameId: 'igHesSmHiFxW7HQYG'},
//     // buttons: [{"id": "id1", "text": "button1", "icon": "ic_menu_share"}, {"id": "id2", "text": "button2", "icon": "ic_menu_send"}]
// };
// Meteor.call('sendNotification', ['xHvq3pw9JobFHaPdb'], data);