import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter, Route, Switch } from 'react-router-dom'
import { createBrowserHistory } from 'history';
import { Meteor } from 'meteor/meteor';
import Games from '../imports/collections/games';
import Categories from '../imports/collections/categories';


// Containers
import Full from '../imports/ui/containers/Full/'

// Views
import Login from '../imports/ui/views/Pages/Login/'
import Register from '../imports/ui/views/Pages/Register/'
import Page404 from '../imports/ui/views/Pages/Page404/'
import Page500 from '../imports/ui/views/Pages/Page500/'

const history = createBrowserHistory();

const App = () => {
  return (
    <HashRouter history={history}>
      <Switch>
        <Route exact path="/login" name="Login Page" component={Login}/>
        <Route exact path="/register" name="Register Page" component={Register}/>
        <Route exact path="/404" name="Page 404" component={Page404}/>
        <Route exact path="/500" name="Page 500" component={Page500}/>
        <Route path="/" name="Home" component={Full}/>
      </Switch>
    </HashRouter>
  );
}

Meteor.startup(() => {
  ReactDOM.render((
    <App />
  ), document.getElementById('app'))
});
